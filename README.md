# Super Simple Gallery

### Basics
Drop-dead simple gallery for all those pictures laying around. Low load for older devices. Uses two different scripts and a config file. One script is a pre-processor that scans your pictures and chooses a specified number of images for the gallery. The second is an html page (with vue.js) that runs through the slideshow of chosen images.

### Features
JSON config file for kinda easy preference changes. Lets you pick the number of pics you want to show in each gallery session and how often the slideshow changes images among other things.

### Plans
Get off cron
